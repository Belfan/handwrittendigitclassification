#  HandwrittenDigitClassification with CNN (using Keras) :

This project is about MNIST classification. It is composed of four parts :

## 1.Data preparation : 

In this part:
- I import the Data
- I reshape it to have the appropriate dimensions
- I reserve datas for validation

## 2.Model building and training

The model architecture is the following :
 
CONV-->MAXPOOL-->CONV-->MAXPOOL-->CONV-->FLATTEN-->SOFTMAX

## 3.Evaluation :

In this part i test the trained model on the test dataset.

##  4.Display of some predictions : 

Finally i display some images with its repesctive predictions.
